FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SYSTEMD_AUTO_ENABLE_${PN}-agent = "enable"
SRC_URI = "git://github.com/rancher/k3s.git;branch=release-1.21;name=k3s \
           file://k3s.service \
           file://k3s-agent.service \
           file://k3s-agent \
           file://k3s-clean \
           file://cni-containerd-net.conf \
           file://0001-Finding-host-local-in-usr-libexec.patch;patchdir=src/import \
          "
SRCREV_k3s = "fceb20fe0c71ffd637082215a67a4954c5f3da72"
PV = "v1.21.3+k3s1"
