DESCRIPTION = "Image with SMARTER, a cloud native edge foundation.  \
The image includes k3s and necessary support to run in edge environments."

IMAGE_FEATURES += " ssh-server-openssh package-management post-install-logging"

DISTRO_FEATURES += " systemd virtualization kvm"

IMAGE_INSTALL_append = "\
    kernel-modules \
    k3s-agent \
    rng-tools \
    ntp \
    ca-certificates jq \
    cni \
    bash \
    usbutils \
    daemontools \
    cryptsetup \
    btrfs-tools \
    e2fsprogs \
    e2fsprogs-resize2fs \
    dpkg \
    apt \
    sharutils \
    systemd-conf \
    smarter-config \ 
    "

LICENSE = "MIT"

PV="v2.0.0"
PR="rc1"

inherit core-image


