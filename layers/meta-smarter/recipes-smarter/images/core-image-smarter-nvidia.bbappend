tegraflash_custom_post_tegra194_append() {
   cat <<EOF | tee ${WORKDIR}/flash.xml.patch
@@ -626,6 +626,16 @@
             <description> **Optional.** Reserved for future use by the recovery filesystem;
               removable. </description>
         </partition>
+        <partition name="SMARTER-CFG" type="data">
+            <allocation_policy> sequential </allocation_policy>
+            <filesystem_type> basic </filesystem_type>
+            <size> 16834 </size>
+            <file_system_attribute> 0 </file_system_attribute>
+            <allocation_attribute> 0x8 </allocation_attribute>
+            <percent_reserved> 0 </percent_reserved>
+            <filename> smarter.cfg </filename>
+            <description> **Optional.** SMARTER Configuration Environment. </description>
+        </partition>
         <partition name="UDA" type="data">
             <allocation_policy> sequential </allocation_policy>
             <filesystem_type> basic </filesystem_type>
             
EOF
    patch ${WORKDIR}/tegraflash/flash.xml.in ${WORKDIR}/flash.xml.patch
    cat > ${WORKDIR}/tegraflash/smarter-doflash.sh <<END
#!/bin/bash
K3S_ARGS=2
BYOC_FILE=''
THIS_FILE=\$0

ARGS=\$(getopt -n $(basename "\$0") -l "k3s-master:,k3s-token:,hostname:,sshkey:,passwd:,byoc:,factory" -o "" -- "\$@")
if [ \$? -ne 0 ]; then
    echo "Error parsing options" >&2
    exit 1
fi
eval set -- "\$ARGS"
unset ARGS

# blank config file
echo "# SMARTER Environment Variables" > smarter.cfg

while true; do
    case "\$1" in
        --k3s-master)
            echo "K3S_SERVER_IP=\$2" >> smarter.cfg
            K3S_ARGS=\`expr \$K3S_ARGS - 1\`
            shift 2
            ;;
        --k3s-token)
            echo "K3S_SERVER_TOKEN=\$2" >> smarter.cfg
            K3S_ARGS=\`expr \$K3S_ARGS - 1\`
            shift 2
            ;;
        --hostname)
            echo "FORCE_HOSTNAME=\$2" >> smarter.cfg
            shift 2
            ;;
        --sshkey)
	        echo "SSHKEY=\"\`cat \$2\`\"" >> smarter.cfg
            shift 2
            ;;
        --passwd)
	        echo "PASSWD=\$2" >> smarter.cfg
            shift 2
            ;;
        --byoc)
            BYOC_FILE=\$2
            shift 2
            ;;
        --factory)
            echo "FACTORY_MODE=1" >> smarter.cfg
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Error processing options" >&2
            exit 1
            ;;
    esac
done
if [ \$K3S_ARGS -gt 0 ]; then
    echo "Not enough K3S args for a successfull connection"
    exit
fi
if [ ! -z "\$BYOC_FILE" ]; then
    echo IFS=\'\' read -r -d \'\' String \<\<\"EOF\" >> smarter.cfg
    uuencode \$BYOC_FILE byoc.cert >> smarter.cfg
    echo EOF >> smarter.cfg
fi
shift \$((OPTIND -1))
exec ./doflash.sh "\$@"
END
    chmod +x ${WORKDIR}/tegraflash/smarter-doflash.sh
    wget https://gitlab.com/arm-research/smarter/meta-smarter/-/raw/zeus/README -O ${WORKDIR}/tegraflash/README.txt
}

# TODO: Need to fix: from internal build
#tegraflash_custom_pre_tegra194() {
#    cp ${DEPLOY_DIR_IMAGE}/tegra-minimal-initramfs-jetson-xavier.bup-payload bl_update_payload
#}
