FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"

SRC_URI += "file://skip-veth-interfaces.conf"

do_install_append() {
    install -m 0644 ${WORKDIR}/skip-veth-interfaces.conf ${D}${sysconfdir}/systemd/network/50-veth.network
}

FILES_${PN} += "${sysconfdir}/systemd/network"
