
FILESEXTRAPATHS_prepend:="${THISDIR}/files:"
SRC_URI += "file://vault.cfg \
            file://additional_docker.cfg \
            file://additional_kvm.cfg \
"

KERNEL_MODULE_AUTOLOAD += "vhost-net"
